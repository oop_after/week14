/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thanawuth.swingproject;

import java.io.Serializable;

/**
 *
 * @author Windows 10
 */
public class Friend implements Serializable{
    private String Name;
    private int Age;
    private String Gender;
    private String Description;

    public Friend(String Name, int Age, String Gender, String Description) {
        this.Name = Name;
        this.Age = Age;
        this.Gender = Gender;
        this.Description = Description;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    @Override
    public String toString() {
        return "Friend{" + "Name=" + Name + ", Age=" + Age + ", Gender=" + Gender + ", Description=" + Description + '}';
    }
    
    
}
